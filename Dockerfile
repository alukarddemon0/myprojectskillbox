FROM node

RUN mkdir /app
WORKDIR /app

COPY dz/package.json /app
RUN yarn install

COPY dz /app

RUN yarn test
RUN yarn build

EXPOSE 3000

CMD yarn start